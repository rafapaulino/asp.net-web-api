﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class ProdutoController : ApiController
    {
        Produto[] produtos = new Produto[] {
            new Produto() { Codigo = 1, Nome = "iPhone 7S", Categoria = "Celulares", Valor = 1999.99M },
            new Produto() { Codigo = 2, Nome = "Samsung Galaxy S5", Categoria = "Celulares", Valor = 2500.75M },
            new Produto() { Codigo = 3, Nome = "Nokia Lumina", Categoria = "Celulares", Valor = 689.10M },
            new Produto() { Codigo = 4, Nome = "Teclado Microsoft", Categoria = "Informática", Valor = 110.10M },
            new Produto() { Codigo = 5, Nome = "Mouse Microsoft", Categoria = "Informática", Valor = 55.50M },
            new Produto() { Codigo = 6, Nome = "Case Go Pró", Categoria = "Acessórios", Valor = 65.00M },
        };

        [HttpGet]
        public IEnumerable<Produto> ListarTodos()
        {
            return produtos;
        }

        public IHttpActionResult Get(int codigo)
        {
            var produto = produtos.FirstOrDefault(p => p.Codigo == codigo);
            if (produto == null) return NotFound();
            //return Ok(produto);
            return Json<Produto>(produto);
        }
        public IEnumerable<Produto> GetPorCategoria(string categoria)
        {
            return produtos.Where(p => string.Equals(p.Categoria, categoria, StringComparison.OrdinalIgnoreCase));
        }

        //POST = Insert
        public void Post([FromBody] Produto produto)
        {
            //realiza o cadastro de produto com base no parametro recebido
        }

        //PUT = Insert ou Update
        public void Put([FromBody] Produto produto)
        {
            //realiza o cadastro/atualização de produto com base no parametro recebido
        }

        //DELETE = Delete
        public void Delete(int codigo)
        {
            //realiza exclusão de um produto com base no código recebido
        }
    }
}
